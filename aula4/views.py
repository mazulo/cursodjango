# -*- coding: utf-8 -*-
from django.shortcuts import render


def index(request):
    lista = [
        u'Patrick Mazulo',
        u'Nilton Cesar',
        u'Aleph Melo'
    ]
    return render(request, 'aula4/index.html', {'lista': lista})

# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth import login, logout
from aula7.forms import LoginForm


def index(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            #return HttpResponseRedirect(reverse(''))
    else:
        form = LoginForm()
    return render(request, 'aula7/index.html',
        {
            'form': form,
        }
    )

# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def index(request):
    if request.method == 'POST':
        nome = request.POST.get('name', u'não tem nome')
        return HttpResponse(u'O nome dele é %s' % nome)
    else:
        formulario = '''
        <form action="." method="POST">
            <input type="text" name="name" maxlength="100" />
            <button type="submit">Enviar</button>
        </form>
        '''
        return HttpResponse(formulario)


def detail(request, id):
    return HttpResponse(u'O seu id é: %s' % id)

# -*- coding: utf-8 -*-

from django.db import models


class Contato(models.Model):
    nome = models.CharField(max_length=200)
    email = models.EmailField()
    twitter = models.URLField()
    data_nasc = models.DateField()

    def __unicode__(self):
        return self.nome
